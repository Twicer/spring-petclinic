FROM maven:3.5-jdk-11 as BUILD

COPY . .
COPY . /usr/src/app
RUN mvn --batch-mode -f /usr/src/app/pom.xml clean package

FROM openjdk:11-jdk
ENV PORT 8080
EXPOSE 8080
COPY --from=BUILD /usr/src/app/target /opt/target
WORKDIR /opt/target
#RUN ./install.sh package
#RUN bash -c "install.sh package"
ENTRYPOINT java -jar target/*.jar
#CMD ["/bin/bash", "-c", "find -type f -name '*-with-dependencies.jar' | xargs java -jar"]

#FROM ubuntu:latest
#RUN apt update && apt install -y net-tools telnet wget curl
#RUN telnet 127.0.0.1 8080 || wget 127.0.0.1 8080 || curl localhost 8080
